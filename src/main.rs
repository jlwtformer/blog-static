use std::{
    env,
    fs::{self, File},
    io::{self, prelude::*, BufRead},
    path::Path,
};
use walkdir::WalkDir;

fn main() {
    let args: Vec<String> = env::args().skip(1).collect();

    match args.as_slice() {
        [src, dst, title] => process_files(Path::new(src), Path::new(dst), title),
        _ => {
            print!("Usage:\nblog-static src dst title\n");
            println!("{}", args.len());
        }
    }
}

fn process_files(src: &Path, dst: &Path, title: &str) {
    for e in WalkDir::new(src) {
        let e = e.unwrap();
        let path = e.path();

        if path.is_file() {
            match path.extension() {
                Some(extension) if extension == "md" => {
                    let file_contents = fs::read_to_string(path).unwrap();
                    let contents = markdown::to_html_with_options(file_contents.as_str(), &markdown::Options::gfm()).unwrap();
                    let title = render_title(title, path);
                    let contents = render_html_file(&contents, &title);
                    let path = path.strip_prefix(src).unwrap();
                    let new_path = dst.join(path.with_extension("html"));

                    fs::create_dir_all(new_path.parent().unwrap())
                        .expect("Error creating path to new file.");
                    fs::write(&new_path, contents).expect("Error creating new html file.");
                }
                Some(_) => {
                    let new_path = path.strip_prefix(src).unwrap();
                    let new_path = dst.join(new_path);

                    fs::create_dir_all(new_path.parent().unwrap())
                        .expect("Error creating path to new file.");
                    fs::copy(path, new_path).expect("Error copying additional files.");
                }
                None => {}
            }
        }
    }
}

fn render_html_file(contents: &str, title: &str) -> String {
    let mut new_file = String::new();

    if Path::new("header.html").exists() {
        let header = File::open("header.html").unwrap();
        let reader = io::BufReader::new(header);

        for line in reader.lines() {
            let line_contents = line.unwrap();

            if line_contents.trim() == "<title></title>" {
                let new_title = format!("<title>{title}</title>\n");
                new_file.push_str(&new_title);
            } else {
                new_file.push_str(&line_contents);
            }
        }
    } else {
        println!("No header file found");
    }

    new_file.push_str(contents);

    if Path::new("footer.html").exists() {
        let mut footer = File::open("footer.html").unwrap();
        footer.read_to_string(&mut new_file).unwrap();
    } else {
        println!("No footer file found");
    }

    new_file
}

fn render_title(title: &str, path: &Path) -> String {
    let mut new_title = String::new();

    let file = File::open(path).unwrap();
    let mut reader = io::BufReader::new(file);

    reader
        .read_line(&mut new_title)
        .expect("Unable to read title of file.");
    title.to_owned() + &new_title[1..]
}
