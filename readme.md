# bs: blog-static

Blog static is a simple and portable tool that allows a server to convert a batch of markdown files into a static website. Inspired by SSG6, this tool aims to accomplish the same function, but in a more standalone manner.

## Contributing

As this project is open source, it is higly encouraged to contribute via merge request or by reporting any issues.

## License

blog-static is available under the terms of the GNU General Public License Version 3. For full license terms, see LICENSE.

## Donation

If you would like to support the work I am doing, feel free to [donate here](https://ko-fi.com/jwestall)!
