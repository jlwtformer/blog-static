# Using Program With Git Pages

Using this program with git pages is very simple. First, head over to the [releases page](https://gitlab.com/jlwtformer/blog-static/-/releases) and download the compiled program. Next set your pages repo up as follows:

```
Project Root\
- blog-static executable
- header.html
- footer.html
- src\
    - index.md
    - blogs\
        - all blog posts
    - res\
        - any file you want
```

Ideally the order of markdown and other files in the `src\` directory does not matter. It should all copy to the public folder in the same order.

Next, create the `.gitlab-ci.yml` file in your project root. It should look as folows:

```
default:
    image: ubuntu:22.04

pages:
  stage: deploy
  script:
    - mkdir .public
    - ./blog-static src .public 'Blog Title'
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
```

Make sure to change the `Blog Title` to whatever the title of your blog is. Ensure you are selecting a docker image that is based on a stable Linux distribution. Some of the default images will not process the blog-static binary properly.